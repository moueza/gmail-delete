package services;

import java.util.List;

import entities.Mail;
public interface MailServices {
public List<Mail> getNumberOfMails();

public List<Mail> getNumberOfMailsNOTstarred();



/**ex : efface 15% des derniers mails
 * @return */
	public void deletePercentMails();
	
	/**ex : delete 15% des derniers mails non starred
	 * @return */
		public void deletePercentMailsNOTstarred();
}
