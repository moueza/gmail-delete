package services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Properties;

//import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

//http://techqa.info/programming/tag/encode?after=36953296
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
//import org.apache.commons.codec.binary.Base64;
import java.nio.charset.StandardCharsets;




//https://developers.google.com/gmail/api/v1/reference/users/messages/send
//import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;

/**Gmail API  My Project	protean-tooling-660
 * https://console.developers.google.com/cloud-resource-manager?previousPage=%2Fiam-admin%2Froles%2Fproject%3Fproject%3Dgmail-delete-java%26hl%3Dfr&hl=fr*/

//import javax.mail.internet;
public class Main {
	// https://developers.google.com/gmail/api/guides/sending
	public static void main(String[] args) throws MessagingException, IOException {
		Main main = new Main();
		//MimeMessage mesg = Main.createEmail("synhedionn@gmail.com", "mouezapeter@gmail.com", "my subject", "body text");
	Message message =	Main.createMessageWithEmail3(mesg);
		
	Gmail gmailService = new Gmail();
	message.sendMessage(gmailService,
                 "186000324771-pfddk0vmmqa3f511hlmpgv0f157liokp.apps.googleusercontent.com",
                 message);
		 }

	// https://developers.google.com/gmail/api/guides/sending
	/**
	 * Create a MimeMessage using the parameters provided.
	 *
	 * @param to
	 *            email address of the receiver
	 * @param from
	 *            email address of the sender, the mailbox account
	 * @param subject
	 *            subject of the email
	 * @param bodyText
	 *            body text of the email
	 * @return the MimeMessage to be used to send email
	 * @throws MessagingException
	 */
	public static MimeMessage createEmail(String to, String from, String subject, String bodyText)
			throws MessagingException {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		MimeMessage email = new MimeMessage(session);

		email.setFrom(new InternetAddress(from));
		email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to));
		email.setSubject(subject);
		email.setText(bodyText);
		return email;
	}

	/**
	 * Create a message from an email.
	 *
	 * @param emailContent
	 *            Email to be set to raw of message
	 * @return a message containing a base64url encoded email
	 * @throws IOException
	 * @throws MessagingException
	 */
	// public static Message createMessageWithEmail(MimeMessage emailContent)
	// throws MessagingException, IOException {
	// ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	// emailContent.writeTo(buffer);
	// byte[] bytes = buffer.toByteArray();
	//
	//
	// //String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
	// String encodedEmail = Base64.getEncoder()
	// Message message = new Message();
	// message.setRaw(encodedEmail);
	//
	//
	// return message;
	// }

	/**
	 * Create a message from an email.
	 *
	 * @param emailContent
	 *            Email to be set to raw of message
	 * @return a message containing a base64url encoded email
	 * @throws IOException
	 * @throws MessagingException
	 */
	public static void createMessageWithEmail2(MimeMessage emailContent) throws MessagingException, IOException {

		/**
		 * java8
		 * https://stackoverflow.com/questions/14413169/which-java-library-provides-base64-encoding-decoding
		 */
		byte[] message2 = "hello world".getBytes(StandardCharsets.UTF_8);
		String encoded = Base64.getEncoder().encodeToString(message2);
		byte[] decoded = Base64.getDecoder().decode(encoded);
		System.out.println(encoded);
		System.out.println(new String(decoded, StandardCharsets.UTF_8));

	}

	/**
	 * Create a message from an email.
	 *
	 * @param emailContent
	 *            Email to be set to raw of message
	 * @return a message containing a base64url encoded email
	 * @throws IOException
	 * @throws MessagingException
	 */
	public static Message createMessageWithEmail3(MimeMessage emailContent) throws MessagingException, IOException {

		/**
		 * java8
		 * https://stackoverflow.com/questions/14413169/which-java-library-provides-base64-encoding-decoding
		 */
		byte[] message2 = "hello world".getBytes(StandardCharsets.UTF_8);
		String encoded = Base64.getEncoder().encodeToString(message2);
		//byte[] decoded = Base64.getDecoder().decode(encoded);
		// System.out.println(encoded);
		// System.out.println(new String(decoded, StandardCharsets.UTF_8));

		// from version 0
		// Message message = new Message();
		// message.setRaw(encodedEmail);
		Message message=new Message();
		//emailContent.setr
		message.setRaw(encoded); //setRaw(encoded);
		//
		//
		return message;
	}
	
	
	 /**
     * Send an email from the user's mailbox to its recipient.
     *https://developers.google.com/gmail/api/guides/sending
     * @param service Authorized Gmail API instance.
     * @param userId User's email address. The special value "me"
     * can be used to indicate the authenticated user.
     * @param emailContent Email to be sent.
     * @return The sent message
     * @throws MessagingException
     * @throws IOException
     */
    public static Message sendMessage(Gmail service,
                                      String userId,
                                      MimeMessage emailContent)
            throws MessagingException, IOException {
        Message message = createMessageWithEmail3(emailContent);
        message = service.users().messages().send(userId, message).execute();

        System.out.println("Message id: " + message.getId());
        System.out.println(message.toPrettyString());
        return message;
    }
}
